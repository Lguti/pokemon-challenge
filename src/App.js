import './App.sass';
import React from 'react';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

//  Custom Components
import HomePage from './components/layout/HomePage';
import PokemonProfile from './components/profile/PokemonProfile';
import Header from './components/layout/Header';

const App = () => {
  return (
    <Router>
      <Switch>
        <Route exact path='/' component={ HomePage } />
        <Route exact path='/:name' component={ PokemonProfile } />
        <Route>
          <Header message={'Page not found'} />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
