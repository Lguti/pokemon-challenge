import axios from "axios";

//  Base Pokemon Url
const BASE_URL_POKEMON = 'https://pokeapi.co/api/v2/pokemon';

//  Base Pokemon Url with pagination
const URL_POKEMON_PAGINATE = 'https://pokeapi.co/api/v2/pokemon?limit=5&offset=0';

//  Initial state 
export const iniState = {
    loading: true,
    data: [],
    next: '',
    previous: '',
    count: 0,
    error: ''
}

//  Initital state for profile
export const initStateProfile = { 
    loading: true,
    error: ''
};

//  Initial state for details
export const initStateDetails = {
    location: [], 
    egg_groups: [], 
    evolves_from: '', 
    habitat: '', 
    shape: '',
    error: ''
}

//  Get pokemons paginate by 5
export const getPokemons = async (url = URL_POKEMON_PAGINATE) => {
    try {
        const res = await axios.get(url);
        return res.data;
    } catch (err) {
        throw err.response.statusText;
    }
}

//  Get individual data 
export const getIndividualPokemonImage = async url => {
    try {
        const res = await axios.get(url);
        return res.data.sprites.other.dream_world.front_default;
    } catch (err) {
        throw err.response.statusText;
    }
}

//  Get data by name
export const getPokemonByName = async name => {
    try {
        const res = await axios.get(`${BASE_URL_POKEMON}/${name}`);
        return res.data;
    } catch (err) {
        throw err.response.data;
    }
}

//  Get data by url
export const getDataByUrl = async url => {
    try {
        const res = await axios.get(url);
        return res.data;
    } catch (err) {
        throw err.response.statusText;
    }
}

//  Validate image and return an array 
export const validateImages = sprites => {
    let images = [];

    for (const key in sprites) {
        if (Object.hasOwnProperty.call(sprites, key)) {
            const element = sprites[key];
            if (element && typeof element !== 'object') {
                images.push(element);
            }
        }
    }

    for (const key in sprites.other.dream_world) {
        if (sprites.other.dream_world.hasOwnProperty.call(sprites.other.dream_world, key)) {
            const element = sprites.other.dream_world[key];
            if (element) {
                images.push(element);
            }
        }
    }
    
    return images;
}

//  Type color
export const typeColors = {
    bug: '#729f3f',
    dragon: '#53a4cf',
    fairy: '#fdb9e9',
    fire: '#fd7d24',
    ghost: '#7b62a3',
    ground: '#f7de3f',
    normal: '#a4acaf',
    psychic: '#f366b9',
    steel: '#9eb7b',
    dark: '#707070',
    electric: '#eed535',
    fighting: '#d56723',
    flying: '#3dc7ef',
    grass: '#9bcc50',
    ice: '#51c4e7',
    poison: '#b97fc9',
    rock: '#a38c21',
    water: '#4592c4'
}