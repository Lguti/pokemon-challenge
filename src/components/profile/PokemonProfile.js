import React, { useState, useEffect, Fragment } from 'react'
import { useParams } from "react-router-dom";

//  PokeApi stuff
import { getPokemonByName, initStateProfile } from '../../utils/pokeApi';

//  Custom Components
import PokeLoading from '../layout/PokeLoading';
import Header from '../layout/Header';
import ProfileContent from './ProfileContent';


const PokemonProfile = () => {
    //  Set init state for profile
    const [profile, setProfile] = useState(initStateProfile);
    const [isActive, setIsActive] = useState({ first: true, second: false, thrid: false })

    //  Get name from url
    const { name } = useParams();

    useEffect(() => {
        //  Get Pokemon info
        getPokemonByName(name)
        .then(pokemon => setProfile({ ...profile, pokemon, loading: !profile.loading }))
        .catch(err => setProfile({ ...profile, loading: !profile.loading, error: err }))

        //  Clean up
        return () => setProfile(initStateProfile);
    }, [name])

    return (
        <Fragment>
            {
                //  Validate if exist pokemon data
                !profile.loading && profile.pokemon ? 
                (
                    <>
                        <Header message={`${isActive.first ? 'Details' : isActive.second ? 'Images' : 'Abilities'} of ${name}`} miniImage={profile.pokemon.sprites.front_default} />
                        <section className="container section is-small">
                            <ProfileContent profile={profile.pokemon} activeTab={isActive} changeActive={setIsActive} />
                        </section>
                    </>
                ) :
                //  Validate 404
                !profile.loading && profile.error ? 
                (
                    <>
                        <Header message={profile.error} />
                    </>
                ) :
                //  Meanwhile loading
                (
                    <section className="container section is-medium">
                        <PokeLoading />
                    </section>
                )
            }
        </Fragment>
    )
}

export default PokemonProfile
