import React from 'react';
import PropTypes from 'prop-types';

//  PokeApi stuff
import { validateImages } from '../../utils/pokeApi';

const PokemonImages = ({ sprites }) => {
    const images = validateImages(sprites);
    return (
        <section className="container">
            <div className="columns flex-wrap">
                {
                    images.map((img, index) => (
                        <div key={index} className='column is-2'>
                            <figure className='image is-128x128'>
                                <img className='is-rounded' src={img} alt='front_default'/>
                            </figure>
                        </div>
                    ))
                }
            </div> 
        </section>
    )
}

PokemonImages.propTypes = {
    sprites: PropTypes.object.isRequired
}

export default PokemonImages
