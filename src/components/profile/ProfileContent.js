import React, { Fragment } from 'react'
import PropTypes from 'prop-types'

//  Custom components
import PokemonAbility from './PokemonAbility';
import PokemonDetail from './PokemonDetail';
import PokemonImages from './PokemonImages';

const ProfileContent = ({ activeTab, changeActive, profile }) => {
    //  Set each page depend on active tab 
    const activePage = ({ first, second, thrid }) => {
        let page;
        switch (true) {
            case first:
                page = <PokemonDetail profile={profile}/>
                break;
            case second:
                page = <PokemonImages sprites={profile.sprites} />
                break;
            case thrid:
                page = <PokemonAbility abilities={profile.abilities} moves={profile.moves.slice(0, 18)} />
                break;
            default:
                page = <PokemonDetail profile={profile}/>
                break;
        }
        return page
    }

    return (
        <Fragment>
            <div className="tabs is-medium is-boxed">
                <ul>
                    <li className={activeTab.first ? 'is-active' : ''}>
                        <a onClick={e => changeActive({ first: true, second: false, thrid: false })}>Details</a>
                    </li>
                    <li className={activeTab.second ? 'is-active' : ''}>
                        <a onClick={e => changeActive({ first: false, second: true, thrid: false })}>Images</a>
                    </li>
                    <li className={activeTab.thrid ? 'is-active' : ''}>
                        <a onClick={e => changeActive({ first: false, second: false, thrid: true })}>Abilities</a>
                    </li>
                </ul>
            </div>
            {activePage(activeTab)}
        </Fragment>
    )
}

ProfileContent.propTypes = {
    activeTab: PropTypes.object.isRequired,
    changeActive: PropTypes.func.isRequired,
    profile: PropTypes.object.isRequired

}

export default ProfileContent
