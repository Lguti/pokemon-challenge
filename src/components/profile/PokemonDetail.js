import React, { Fragment, useEffect, useState } from 'react';
import PropTypes from 'prop-types';

//  PokeApi stuff
import { typeColors, getDataByUrl, initStateDetails } from '../../utils/pokeApi'

const PokemonDetail = ({ 
    profile: {
        name, 
        types, 
        weight, 
        height, 
        base_experience, 
        stats, 
        location_area_encounters, 
        species 
    } 
}) => {
    const [data, setData] = useState(initStateDetails)

    useEffect(() => {
        getDataByUrl(location_area_encounters)
        .then(locations => setData({...data, location: locations}))
        .catch(err => setData({ ...data, error: err }))

        getDataByUrl(species.url)
        .then(rest => setData({
            ...data,
            egg_groups: rest.egg_groups,
            evolves_from: rest.evolves_from_species.name,
            habitat: rest.habitat.name,
            shape: rest.shape.name
        }))
        .catch(err => setData({ ...data, error: err }))

        //  Clean up
        return () => setData(initStateDetails);

    }, []);

    return (
        <section className="container">
            <h1 className="title mb-3 is-capitalized">{name}</h1>
            <div className="columns flex-wrap">
                <div className="column is-full">
                    <div className="box has-background-white-ter">
                        <div className="columns flex-wrap">
                            <div className="column is-2">
                                <article className="media">
                                    <div className="content">
                                        <p className="subtitle">
                                            <strong className="subtitle">Attack</strong>
                                            <br/><br/>
                                            {stats[1].base_stat} xp
                                            <br/><br/>
                                            <strong className="subtitle">Defense</strong>
                                            <br/><br/>
                                            {stats[2].base_stat} xp
                                        </p>
                                    </div>
                                </article>
                            </div>
                            <div className="column is-2">
                                <article className="media">
                                    <div className="content">
                                        <p className="subtitle">
                                            <strong className="subtitle">Special Attack</strong>
                                            <br/><br/>
                                            {stats[3].base_stat} xp
                                            <br/><br/>
                                            <strong className="subtitle">Speed</strong>
                                            <br/><br/>
                                            {stats[5].base_stat}
                                        </p>
                                    </div>
                                </article>
                            </div>
                            <div className="column is-2">
                                <article className="media">
                                    <div className="content">
                                        <p className="subtitle">
                                            <strong className="subtitle">Weight</strong>
                                            <br/> <br/>
                                            {/* Weight came in hetograms, transform to kilograms */}
                                            {weight/10} Kg
                                            <br/><br/>
                                            <strong className='subtitle'>Height</strong>
                                            <br/><br/>
                                            {/* Height came in decimeters, transform to meters */}
                                            {height/10} M
                                        </p>
                                    </div>
                                </article>
                            </div>
                            <div className="column is-2">
                                <article className="media">
                                    <div className="content">
                                        <p className="subtitle">
                                            <strong className="subtitle">Location area</strong>
                                            <br/><br/>
                                            {
                                                data.location.length > 0 ? data.location[0].location_area.name : 
                                                'Not available'
                                            }
                                            <br/><br/>
                                            <strong className="subtitle">Eggs groups</strong>
                                            <br/><br/>
                                            {
                                                data.egg_groups.length > 0 ? data.egg_groups.map((group, index) => 
                                                    index === 0 ? group.name : ` | ${group.name}`
                                                ) :
                                                'Not available'
                                            }
                                        </p>
                                    </div>
                                </article>
                            </div>
                            <div className="column is-2">
                                <article className="media">
                                    <div className="content">
                                        <p className="subtitle">
                                            <strong className="subtitle">Evolves from</strong>
                                            <br/><br/>
                                            {
                                                data.evolves_from ? data.evolves_from : 'Not available'
                                            }
                                            <br/><br/>
                                            <strong className="subtitle">Habitat</strong>
                                            <br/><br/>
                                            {
                                                data.habitat ? data.habitat : 'Not available'
                                            }
                                        </p>
                                    </div>
                                </article>
                            </div>
                            <div className="column is-2">
                                <article className="media">
                                    <div className="content">
                                        <p className="subtitle">
                                            <strong className="subtitle">Shape</strong>
                                            <br/><br/>
                                            {
                                                data.shape ? data.shape : 'Not available'
                                            }
                                            <br/><br/>
                                            <strong className="subtitle">Experience</strong>
                                            <br/><br/>
                                            {base_experience} xp
                                        </p>
                                    </div>
                                </article>
                            </div>
                            <div className="column is-2">
                                <article className="media">
                                    <div className="content">
                                        <p className="subtitle">
                                            <strong className="subtitle">Type</strong>
                                            {
                                                types.map(data => (
                                                    <Fragment key={data.type.name}>
                                                        <br/><br/> 
                                                        <b className='has-text-white-ter p-2 rounded-8' style={{ backgroundColor: typeColors[data.type.name] }}>{data.type.name} </b>
                                                    </Fragment>
                                                ))
                                            }
                                        </p>
                                    </div>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

PokemonDetail.propTypes = {
    profile: PropTypes.object.isRequired
}

export default PokemonDetail
