import React from 'react'
import PropTypes from 'prop-types'

const PokemonAbility = ({ abilities, moves }) => {
    return (
        <section className="container">
            <h1 className="subtitle mb-3">Abilities</h1>
            <div className="columns">
            {
                abilities.map(data => (
                    <div key={data.ability.name} className="column is-2">
                        <div className='card has-background-link-dark'>
                            <div className='card-content d-flex justify-content-around'>
                                <div className="content">
                                    <p className='subtitle'>
                                        <strong className='has-text-white'>{data.ability.name}</strong>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                ))
            }
            </div>
            <h1 className="subtitle mb-3">Some Moves</h1>
            <div className="columns flex-wrap">
                {
                    moves.map(({ move }) => (
                        <div key={move.name} className="column is-2">
                            <div className='card has-background-link-dark'>
                                <div className='card-content d-flex justify-content-around'>
                                    <div className="content">
                                        <p className='subtitle'>
                                            <strong className='has-text-white'>{move.name}</strong>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    ))
                }
            </div>
        </section>
    )
}

PokemonAbility.propTypes = {
    abilities: PropTypes.array.isRequired,
    moves: PropTypes.array.isRequired
}

export default PokemonAbility
