import React from 'react';
import PropTypes from 'prop-types';

const Pagination = ({ goToPrevious, goToNext, previous, totalPage, count }) => {
    return (
        <nav className="pagination is-centered" role="navigation" aria-label="pagination">
            <button onClick={e => goToPrevious()} className="button pagination-previous" disabled={!previous ? true : false}>Previous</button>
            <button onClick={e => goToNext()} className="button pagination-next">Next page</button>
            <ul className='pagination-list'>
                <li><button className='button' disabled>{count}</button></li>
                <li><span className="pagination-ellipsis">&hellip;</span></li>
                <li><button className='button' disabled="disabled">{totalPage}</button></li>
            </ul>
        </nav>
    )
}

Pagination.prototype = {
    goToPrevious: PropTypes.func.isRequired,
    goToNext: PropTypes.func.isRequired,
    previous: PropTypes.bool.isRequired,
    totalPage: PropTypes.number.isRequired,
    count: PropTypes.number.isRequired
}

export default Pagination
