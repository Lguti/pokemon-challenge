import React, { Fragment, useState, useEffect } from 'react'

//  PokeApi stuff
import { getIndividualPokemonImage, getPokemons, iniState } from '../../utils/pokeApi';

//  Custom Components
import Header from './Header';
import Pagination from './Pagination';
import PokermonCard from './PokemonCard';
import PokeLoading from './PokeLoading';


const HomePage = () => {
    //  Set init state for pokemons
    const [pokemons, setPokemons] = useState(iniState)
    const [count, setCount] = useState(1)

    useEffect(() => {
        //  Get Pokemons for principal view
        getPokemons()
        .then(pokemonsData => {
            //  Fetch url looking for the pokemon's image
            pokemonsData.results.forEach(poke => {
                getIndividualPokemonImage(poke.url)
                .then(image => {
                    //  Set Pokemon image
                    poke.image = image;
                    //  Set data in state
                    setPokemons({
                        ...pokemons, 
                        data: pokemonsData.results, 
                        next: pokemonsData.next, 
                        previous: pokemonsData.previous,
                        count: pokemonsData.count,
                        loading: !pokemons.loading
                    })
                })
                .catch(err => setPokemons({ ...pokemons, error: err, loading: false }))
            });
        })
        .catch(err => setPokemons({ ...pokemons, error: err, loading: false }));

        //  Clean up
        return () => setPokemons(iniState)
    }, []);

    //  Handle previous button event
    const handlePrevious = () => getPokemons(pokemons.previous)
    .then(pokemonsData => {
        //  Fetch url looking for the pokemon's image
        pokemonsData.results.forEach(poke => {
            getIndividualPokemonImage(poke.url)
            .then(image => {
                //  Set Pokemon image
                poke.image = image;
                //  Set data in state
                setPokemons({
                    ...pokemons, 
                    data: pokemonsData.results, 
                    next: pokemonsData.next, 
                    previous: pokemonsData.previous,
                    count: pokemonsData.count
                })
                setCount(count - 1)
            })
            .catch(err => setPokemons({ ...pokemons, error: err, loading: false }))
        });
    })
    .catch(err => setPokemons({ ...pokemons, error: err, loading: false }));

    //  Handle next button event
    const hanfdleNext = () => getPokemons(pokemons.next)
    .then(pokemonsData => {
        //  Fetch url looking for the pokemon's image
        pokemonsData.results.forEach(poke => {
            getIndividualPokemonImage(poke.url)
            .then(image => {
                //  Set Pokemon image
                poke.image = image;
                //  Set data in state
                setPokemons({
                    ...pokemons, 
                    data: pokemonsData.results, 
                    next: pokemonsData.next, 
                    previous: pokemonsData.previous,
                    count: pokemonsData.count
                })
                setCount(count + 1)
            })
            .catch(err => setPokemons({ ...pokemons, error: err, loading: false }))
        });
    })
    .catch(err => setPokemons({ ...pokemons, error: err, loading: false }));

    return (
        <Fragment>
        {
            !pokemons.loading && pokemons.data.length > 0 ? 
            (
            <>
                <Header message={`Number of Pokemons found: ${pokemons.count}`}/> 
                <section className="container section is-small">
                    <h1 className="title mb-3">Pokemons List</h1>
                    <div className="columns">
                    {
                        pokemons.data.map(pokemon => <PokermonCard key={pokemon.name} image={pokemon.image} name={pokemon.name}>{pokemon.name}</PokermonCard>)
                    }
                    </div>
                    <Pagination count={count} goToPrevious={handlePrevious} goToNext={hanfdleNext} previous={pokemons.previous ? true : false} next={pokemons.next} totalPage={Math.trunc(pokemons.count / 5)}/>
                </section>
            </>
            ) :
            !pokemons.loading && pokemons.error ? 
            (
                <>
                    <Header message={pokemons.error}/>
                </>
            )
            : 
            (
            <section className="container section is-medium">
                <PokeLoading />
            </section>
            )
        }
        </Fragment>
    )
}

export default HomePage
