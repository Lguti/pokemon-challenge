import React from 'react';
import PropTypes from 'prop-types';
import { Link } from "react-router-dom";


const PokemonCard = ({ image, name }) => 
<div className='column'>
    <div className="card">
        <div className='card-image'>
            <Link to={`/${name}`}>
                <figure className='image is-square'>
                    <img src={image} alt={name}/>
                </figure>
            </Link>
        </div>
    </div>
</div>

PokemonCard.prototype = {
    image: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired
}

export default PokemonCard
