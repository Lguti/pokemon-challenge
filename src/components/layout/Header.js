import React from 'react';
import PropTypes from 'prop-types';
import Pokeball from '../../img/pokeball-icon.png';
import { Link } from 'react-router-dom';


const Header = ({ message, miniImage }) => 
<header className="hero is-danger is-small">
    <div className="hero-body">
        <div className='d-flex-column align-items-center justify-content-around'>
            <Link to='/'>
                <figure className='image is-64x64'>
                    <img className='is-rounded' src={Pokeball} alt="pokeball"/>
                </figure>
            </Link>
            <p className={miniImage ? 'title mb-0' : 'title'}>Pokemon's challenge</p>
            <div className='d-flex align-items-center flex-direction-row-reverse'>
                {
                    miniImage && (
                        <figure className='image is-96x96'>
                            <img src={miniImage} alt="logo"/>
                        </figure>
                    )
                }
                <p className="subtitle">
                    <strong>
                        {message}
                    </strong>
                </p>
            </div>
        </div>
    </div>
</header>

Header.prototype = {
    message: PropTypes.string.isRequired,
    miniImage: PropTypes.string
}

export default Header
