import { render, screen } from '@testing-library/react';
import App from './App';
import { getPokemons } from './utils/pokeApi';

test('Loading first', () => {
  render(<App />);
  screen.getByText((content, element) => {
    return element.tagName.toLocaleLowerCase() === 'div' && element.classList.contains('pokeball')
  })
})

test('Get pokemons and Pokemon count equal to 5', async () => {
  try {
    const res = await getPokemons();
    expect(res.results.length).toEqual(5);
  } catch (error) {
    expect(error.toLocaleLowerCase()).toEqual("not found");
  }
})

