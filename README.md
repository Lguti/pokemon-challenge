# Pokemon Challenge :rocket: 

## **Install** dependencies first :cop: :warning:

- [x] ``` npm install ```

## Next

- [x] ``` npm start ``` or ``` yarn start ```
- [x]  **The App is running on port 3000** 

## Highlight :fire: :fire: 

- [x]  **List Pokemon's cards paginate by 5** 
- [x]  **See detail when you click any card** 
- [x]  **Responsive styles** 
- [ ]  **Should have at least 2 language changes** 
- [ ]  **Make something brutally amazing** 

## Unit/Functional Testing :skull: :skull:

- [ ] **Testing the app with jest**
